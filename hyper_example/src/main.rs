//use tokio::io::AsyncReadExt;
//use tokio_fs::File;

use hyper::service::*;
use hyper::*;

use std::task::{Context, Poll};

// use futures_util::future;
use hyper::service::Service;
use hyper::{Body, Request, Response, Server};

use futures::future::*;

// static INDEX: &str = "example.html";
// static INTERNAL_SERVER_ERROR: &[u8] = b"Internal Server Error";
// static NOTFOUND: &[u8] = b"Not Found";

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let addr = "127.0.0.1:1337".parse().unwrap();

    /*
    let make_service = make_service_fn(|_| async {
        Ok::<_, hyper::Error>(service_fn(response_examples))
    });
    */

    let svc = make_service_fn(|_| async { Ok::<_, Error>(Svc {}) });

    let server = Server::bind(&addr).serve(svc);

    println!("Listening on http://{}", addr);

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

pub struct SkeletonResponse {
    body: String,
}

#[derive(Debug)]
pub struct Svc;

impl Service<Request<Body>> for Svc {
    type Response = Response<Body>;
    // type Response = SkeletonResponse;
    type Error = hyper::Error;
    type Future = std::pin::Pin<
        Box<dyn futures::Future<Output = std::result::Result<Self::Response, Self::Error>> + Send>,
    >;
    // type Future = std::pin::Pin<Box<dyn futures::Future<Output = std::result::Result<Self::Response, Self::Error>> + Send>>;

    fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<std::result::Result<(), Self::Error>> {
        Ok(()).into()
    }

    fn call(&mut self, req: Request<Body>) -> Self::Future {
        async {
            let resp = my_handler(req).await?;

            let mut rsp = Response::builder();
            let body = Body::from(resp.body);
            let rsp = rsp.status(200).body(body).unwrap();
            Ok(rsp)
        }
            .boxed()
    }
}

fn my_handler(
    _req: Request<Body>,
) -> std::pin::Pin<
    Box<dyn futures::Future<Output = std::result::Result<SkeletonResponse, hyper::Error>> + Send>,
> {
    let mut rsp = Response::builder();

    let body = Body::from(Vec::from(&b"heyo!"[..]));
    let _rsp = rsp.status(200).body(body).unwrap();
    Box::pin(futures::future::ok(SkeletonResponse {
        body: "Don't annoy me".to_string(),
    }))
}

/*
async fn response_examples(req: Request<Body>) -> Result<Response<Body>> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/") |
        (&Method::GET, "/index.html") |
        (&Method::GET, "/big_file.html") => {
            simple_file_send(INDEX).await
        }
        (&Method::GET, "/no_file.html") => {
            // Test what happens when file cannot be be found
            simple_file_send("this_file_should_not_exist.html").await
        }
        _ => Ok(not_found())
    }
}

/// HTTP status code 404
fn not_found() -> Response<Body> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body(NOTFOUND.into())
        .unwrap()
}

/// HTTP status code 500
fn internal_server_error() -> Response<Body> {
    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .body(INTERNAL_SERVER_ERROR.into())
        .unwrap()
}

async fn simple_file_send(filename: &str) -> Result<Response<Body>> {
    // Serve a file by asynchronously reading it entirely into memory.
    // Uses tokio_fs to open file asynchronously, then tokio::io::AsyncReadExt
    // to read into memory asynchronously.

    println!("File {}", filename);

    if let Ok(mut file) = File::open(filename).await {
        let mut buf = Vec::new();
        if let Ok(_) = file.read_to_end(&mut buf).await {
            return Ok(Response::new(buf.into()));
        }

        return Ok(internal_server_error());
    }

    return Ok(not_found());
}
*/
