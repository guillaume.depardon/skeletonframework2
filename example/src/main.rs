use backbone::middlewares::cookies::CookieMiddleware;
use backbone::*;

#[tokio::main]
async fn main() {
    println!("Hello, world!");

    let ping_handler = controller!(Pinger { counter: 0 });

    let mut router = default_router();

    router.add(route!(GET, "/what1", ping_handler.pong));
    router.add(route!(GET, "/what2", ping_handler.pong));
    router.add(route!(GET, "/what3", ping_handler.pong));

    let mut web = Web::new(router);
    web.use_around_middlewares(CookieMiddleware::new());
    web.use_around_middlewares(TestMiddleware {});
    web.use_around_middlewares(TestMiddleware2 {});

    web.listen("127.0.0.1:1337".parse().unwrap()).await;
}

pub struct TestMiddleware;
impl AroundMiddleware for TestMiddleware {
    fn around(
        &mut self,
        mut req: &mut SkeletonRequest,
        next: &mut dyn FnMut(&mut SkeletonRequest) -> SkeletonResponse,
    ) -> SkeletonResponse {
        //println!("before");
        let resp = next(&mut req);
        //println!("after");

        resp
    }
}

pub struct TestMiddleware2;
impl AroundMiddleware for TestMiddleware2 {
    fn around(
        &mut self,
        mut req: &mut SkeletonRequest,
        next: &mut dyn FnMut(&mut SkeletonRequest) -> SkeletonResponse,
    ) -> SkeletonResponse {
        //println!("before2");
        let resp = next(&mut req);
        //println!("after2");

        resp
    }
}

pub struct Pinger {
    counter: i32,
}

impl Pinger {
    pub fn pong(&mut self, req: &mut SkeletonRequest) -> SkeletonResponse {
        let cookiejar = req.extensions_mut().get_mut::<cookies::CookieJar>().unwrap();
        cookiejar.add(cookies::Cookie::new("foo", format!("{}", self.counter)));

        let url = req.uri().clone().path().to_string();
        //println!("{} {}", url, self.counter);
        self.counter += 1;


        let mut rsp = Response::builder();

        let body = Body::from(url);
        let response = rsp.status(200).body(body).unwrap();

        fut(response)
    }
}
