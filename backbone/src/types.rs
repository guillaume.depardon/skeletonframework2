// use hyper::service::Service;
// use futures::*;

// pub type RawError = std::error::Error;
pub type RawRequest = hyper::http::Request<hyper::Body>;
pub type RawResponse = hyper::http::Response<hyper::Body>;

pub type ConcurrentError = dyn std::error::Error + Send + Sync;
pub type ResponseResult = std::result::Result<RawResponse, Box<ConcurrentError>>;

type PinnedResponseFuture<'a> = std::pin::Pin<Box<dyn futures::Future<Output = ResponseResult> + Send + 'a>>;

pub type SkeletonRequest = RawRequest;
pub type SkeletonResponse = PinnedResponseFuture<'static>;

pub type Middleware = Box<dyn FnMut(&mut SkeletonRequest) -> SkeletonResponse + Send>;
pub type ErrorMiddleware = Box<dyn FnMut(&ConcurrentError) -> SkeletonResponse + Send + Sync>;

pub enum Route {
    Core(Middleware),
}
