use crate::*;

pub trait AroundMiddleware {
    fn around(
        &mut self,
        req: &mut SkeletonRequest,
        next: &mut dyn FnMut(&mut SkeletonRequest) -> SkeletonResponse,
    ) -> SkeletonResponse;
}

pub struct MiddlewareChain {
    pub handler: Box<dyn AroundMiddleware + Send>,
    pub next: Option<Box<MiddlewareChain>>,
}

impl MiddlewareChain {
    pub fn call(
        &mut self,
        mut req: &mut SkeletonRequest,
        router: &mut (dyn Router + Send),
    ) -> SkeletonResponse {
        match self.next.as_mut() {
            Some(next) => self.handler.around(&mut req, &mut |mut req| {
                next.as_mut().call(&mut req, router)
            }),
            None => self.handler.around(&mut req, &mut |mut req| {
                let resp = router.dispatch(&mut req);
                resp
            })
        }
    }
}
