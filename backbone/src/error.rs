#[derive(Debug)]
pub struct StrError(&'static str);

impl std::fmt::Display for StrError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(self.0)
    }
}

impl std::error::Error for StrError { }

pub fn static_err(s: &'static str) -> Box<dyn std::error::Error + Send + Sync> {
    Box::new(StrError(s))
}