pub mod middleware;
pub mod middlewares;
pub mod router;
pub mod service;
pub mod types;
pub mod error;

pub use middleware::*;
pub use middlewares::*;
pub use router::*;
pub use service::*;
pub use types::*;
pub use error::*;

pub use hyper::body::Body;
pub use hyper::Method;
pub use hyper::Response;
pub use tokio;

pub fn fut(data: hyper::http::Response<hyper::body::Body>) -> SkeletonResponse {
    Box::pin(futures::future::ok(data))
}

/*
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
*/
