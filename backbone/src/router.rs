use std::collections::HashMap;

use crate::*;

pub trait Router {
    fn add_route(&mut self, method: hyper::Method, path: &str, route: Route);
    fn dispatch(&mut self, req: &mut SkeletonRequest) -> SkeletonResponse;
}

pub fn default_router() -> RouterImpl {
    RouterImpl {
        routes: std::collections::HashMap::new(),
    }
}

pub struct RouterImpl {
    routes: HashMap<hyper::Method, HashMap<String, Route>>,
}

impl RouterImpl {
    fn not_found(&self, msg: String) -> SkeletonResponse {
        let mut resp = hyper::Response::new(hyper::body::Body::from(msg));
        *resp.status_mut() = hyper::StatusCode::NOT_FOUND;
        return Box::pin(futures::future::ok(resp));
    }

    pub fn add(&mut self, route: RouteDefinition) {
        self.add_route(route.method, route.path, route.handler)
    }
}

impl Router for RouterImpl {
    fn add_route(&mut self, method: hyper::Method, path: &str, route: Route) {
        if !self.routes.contains_key(&method) {
            self.routes.insert(method.to_owned(), HashMap::new());
        }

        let path_routes = self.routes.get_mut(&method).unwrap();
        if !path_routes.contains_key(&path.to_owned()) {
            path_routes.insert(path.to_string(), route);
        }
    }

    fn dispatch(&mut self, req: &mut SkeletonRequest) -> SkeletonResponse {
        let uri = req.uri();
        let path = uri.path().trim_end();
        let method = req.method();

        let routes = self.routes.get_mut(&method).unwrap();
        if !routes.contains_key(&path.to_owned()) {
            let msg = format!("{} not found in routes", &path.to_owned());
            return self.not_found(msg);
        }

        let route = routes.get_mut(&path.to_owned()).unwrap();
        let response = match route {
            Route::Core(func) => func(&mut *req),
        };

        response
    }
}

pub struct RouteDefinition {
    pub method: hyper::Method,
    pub path: &'static str,
    pub handler: Route,
}

#[macro_export]
macro_rules! route {
    ($http_method:ident, $path:tt, $struct:ident.$method:ident) => {{
        let cloned = std::sync::Arc::clone(&$struct);

        RouteDefinition {
            method: Method::$http_method,
            path: $path,
            handler: Route::Core(Box::new(move |req| cloned.lock().unwrap().$method(req))),
        }
    }};
}

#[macro_export]
macro_rules! controller {
    ($struct:expr) => {
        std::sync::Arc::new(std::sync::Mutex::new($struct));
    };
}
