use futures::prelude::*;

use crate::Router;
use crate::{AroundMiddleware, MiddlewareChain, ErrorMiddleware};
use crate::{SkeletonRequest, ResponseResult};

use std::sync::Arc;
use futures::lock::Mutex;

pub struct Web<R: Router> {
    router: Option<R>,

    around_middlewares: Option<MiddlewareChain>,

    global_error_handler: Option<ErrorMiddleware>,
}

impl<R: Router + Send + 'static> Web<R> {
    pub fn new(router: R) -> Self {
        Web {
            router: Some(router),

            around_middlewares: None,
            global_error_handler: None,
        }
    }

    pub fn use_around_middlewares(&mut self, mw: impl AroundMiddleware + Send + 'static) {
        use std::mem::replace;

        if self.around_middlewares.is_none() {
            replace(
                &mut self.around_middlewares,
                Some(MiddlewareChain {
                    handler: Box::new(mw),
                    next: None,
                }),
            );
        } else {
            let old_handler = self.around_middlewares.take().unwrap();
            let new_handler = MiddlewareChain {
                handler: Box::new(mw),
                next: Some(Box::new(old_handler)),
            };

            replace(&mut self.around_middlewares, Some(new_handler));
        }
    }

    pub async fn listen(&mut self, bind_addr: std::net::SocketAddr) {
        use hyper::server::conn::AddrStream;
        use hyper::service::*;
        use hyper::{Body, Error, Request};

        let router = Arc::new(Mutex::new(self.router.take().unwrap()));

        let context = Arc::new(Mutex::new(RequestContext {
            around_middlewares: self.around_middlewares.take(),
            global_error_handler: self.global_error_handler.take()
        }));

        let make_svc = make_service_fn(move |_socket: &AddrStream| {
            // let remote_addr = socket.remote_addr();

            let request_handler = {
                let shared_router = Arc::clone(&router);
                let shared_context = Arc::clone(&context);

                move |req: Request<Body>| {
                    let request_router = shared_router.clone();
                    let request_context = Arc::clone(&shared_context);

                    async move {
                        let mut router = request_router.lock().await;
                        let mut context = request_context.lock().await;
                        handle_request(&mut *router, &mut context, req).await
                    }
                }
            };

            future::ok::<_, Error>(service_fn(request_handler))
        });

        let server = hyper::server::Server::bind(&bind_addr).serve(make_svc);

        println!("Listening on http://{}", &bind_addr);

        if let Err(e) = server.await {
            eprintln!("server error: {}", e);
        }
    }
}

struct RequestContext {
    pub around_middlewares: Option<MiddlewareChain>,
    pub global_error_handler: Option<ErrorMiddleware>,
}

async fn handle_request(
    router: &mut (impl Router + Send),
    context: &mut RequestContext,
    mut request: SkeletonRequest,
) -> ResponseResult {

    let resp = match &mut context.around_middlewares {
        Some(middlewares) => middlewares.call(&mut request, router).await,
        None => router.dispatch(&mut request).await,
    };

    if let Err(err) = &resp {
        if let Some(handler_func) = &mut context.global_error_handler {
            return handler_func(err.as_ref()).await;
        }
    }

    resp
}
