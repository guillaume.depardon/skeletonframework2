use crate::*;

use futures::prelude::*;

pub use cookie::*;

pub struct CookieMiddleware;

impl CookieMiddleware {
    pub fn new() -> CookieMiddleware {
        CookieMiddleware {}
    }
}

impl AroundMiddleware for CookieMiddleware {
    fn around(
        &mut self,
        mut req: &mut SkeletonRequest,
        next: &mut dyn FnMut(&mut SkeletonRequest) -> SkeletonResponse,
    ) -> SkeletonResponse {
        pub use cookie::*;

        let mut cookie_jar = CookieJar::new();

        if let Some(cookie_headers) = &req.headers().get(hyper::header::COOKIE) {
            let cookie_header_string = cookie_headers.to_str().unwrap().to_string();
            for chunk in cookie_header_string.split(";") {
                if let Ok(cookie) = Cookie::parse(chunk.to_owned()) {
                    cookie_jar.add_original(cookie);
                }
            }
        }

        req.extensions_mut().insert(cookie_jar);

        let resp = next(&mut req);

        if let Some(cookie_jar) = req.extensions().get::<CookieJar>() {
            let cookie_jar = cookie_jar.clone();
            async move {
                let mut resp = resp.await?;
                for c in cookie_jar.delta() {
                    let raw_cookie = c.to_string();

                    if let Ok(value) =
                        hyper::header::HeaderValue::from_bytes(raw_cookie.as_bytes())
                    {
                        resp.headers_mut().append("Set-Cookie", value);
                    }
                }

                Ok(resp)
            }.boxed()
        } else {
            resp
        }
    }
}




/*
impl SkeletonRequest {
    pub fn cookies(&mut self) -> &mut CookieJar {
        self.data
            .get_mut("cookies")
            .and_then(|x| x.downcast_mut())
            .expect("cookies not registered in context")
    }
}
*/
